Version 4
SHEET 1 880 680
WIRE 720 -96 352 -96
WIRE 352 -80 352 -96
WIRE 352 32 352 0
WIRE 352 32 224 32
WIRE 544 32 352 32
WIRE 560 32 544 32
WIRE 224 64 224 32
WIRE 352 128 352 32
WIRE -224 160 -240 160
WIRE -16 160 -224 160
WIRE 224 160 224 144
WIRE 224 160 64 160
WIRE 320 160 224 160
WIRE 720 160 720 -96
WIRE -240 208 -240 160
WIRE 224 208 224 160
WIRE -240 336 -240 288
WIRE 224 336 224 288
WIRE 224 336 -240 336
WIRE 352 336 352 192
WIRE 352 336 224 336
WIRE 720 336 720 240
WIRE 720 336 352 336
WIRE 720 352 720 336
FLAG 720 352 0
FLAG -224 160 vc
FLAG 544 32 vo
SYMBOL voltage 720 144 R0
WINDOW 3 20 99 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value 6
SYMATTR InstName V1
SYMBOL tl431 336 128 R0
SYMATTR InstName U1
SYMBOL res 336 -96 R0
SYMATTR InstName R1
SYMATTR Value 68
SYMBOL res 208 48 R0
SYMATTR InstName R3
SYMATTR Value 470k
SYMBOL res 208 192 R0
SYMATTR InstName R4
SYMATTR Value 1meg
SYMBOL voltage -240 192 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
WINDOW 3 -92 176 Left 2
SYMATTR Value SINE(2.50 .04 1000)
SYMATTR InstName V2
SYMBOL res 80 144 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 10k
TEXT 32 376 Left 2 !.tran 10m uic
TEXT 248 376 Left 2 !.include tl431.mod
