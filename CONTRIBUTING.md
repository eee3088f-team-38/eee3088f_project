The recorces used to manufacture and program the board have been made freely avalible for anyone to use and implement for themselves as long as it is for personal and not commertial use. This includes the circuit diagrams, PCB design layout as well as the custom footprints that were made. 

3rd party tools such as STM32Cube, KiCad and other design sofware will be needed for making any modifications to the existing code and scematics. Copies of the datasheets for the components that were used in the design have been included as a helpful reference when making any harware/software modifications. 

In the event that an error occurs as a result of the design, it should be brought to our attention with as much detail as possible in the Known Errors File so that we can implement a fix. However, we would also appreciate any help such as if a user finds or implements a fix for an error they found. All other helpful contributions are welcome. 
