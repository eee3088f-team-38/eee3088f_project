# Docs Readme
This board is meant to be used as a HAT for an STM32F0 Discovery board. In order to use this HAT as intended, an STM32F0 Discovery board is required. Additionally, the user will need a micro-USB cable for communicating over the onboard USB to UART interface, as well as the appropritate USB cable for programming the STM32 board. 

The board also requires a power source. It can be configured to be powered off of USB, an external 5V source, or an onboard 18650 battery. 

 

The board was designed in KICAD, meaning this software will be required for looking at schematics and design files / any future modifications. 

 

Components were selected with the JLCPCB components library in mind. Therefore it is advisable that the board is manufactured by JLCPCB. The Gerbers, BOM and POSITION files can be found in the production file of the repository. 

 

The firmware for this board was written in the STM32Cube IDE software, and hence it is advisable that this is used for further development. A software for monitoring USB serial communication will be required to extract the data from the board. An example of such software is PuTTY. 

 

In order to run the basic provided firmware, the following needs to be done: 

-Connect the STM32 discovery board to the HAT by plugging it into the female pin headers on the board 

-download a copy of the example firmware from the firmware folder of the project gitlab page, and open it in the STM32Cube IDE environment 

-plug in the STM32 discovery board via its onboard USB (the port on the discovery board, not the micro-USB port on the HAT) 

-In the STM32Cube software, upload the code onto the STM32 board. Monitor the process, ensure that no errors occur 

-Once the upload has completed, unplug the board 

-Configure the HAT to be powered by the preferred method, by placing jumpers on the correct pinheaders. 

-Place jumpers between the following pins in order to have access to all features (for more info review the schematics found in Production, PCB, main): 

	>bridge the current send / current return pins on the debug port (can be removed for 	measuring current 

	>connect the RX and TX pins of the USB to UART interface to ensure that the 		onboard UART converter is used 

	>to bypass reverse polarity protection or under voltage protection, their respective 	bypass pins can be bridged 

-Supply the board with power using the configured method, and wait for a couple of sampling periods to have passed (for testing purposes, it might be useful to change the argument supplied to the Delay_by_sampling_period function in the main loop, as the 60s default is rather long) 

-Once sufficient sampling periods have passed, connect the HAT to a computer via the onboard micro-USB port.  

-Open the serial communications monitoring tool (PuTTY recommended), and configure the baud-rate to the one specified in the USART2_init method. (default = 9600) 

-Set the serial communications monitoring tool to the COM port to which the board was initialized to (the COM port of the device can be found under the Devices and Printers menu in windows) 

-Click connect in the serial communications monitoring tool 

-The demo program will output all recorded data to this serial interface, for the user to review it. 

-In case an onboard 18650 battery is used, this can be charged as follows: While the battery is plugged in, connect the HAT via USB cable to any 5V USB source that can probide >2 amps. This will charge the battery. Check the charge status LED’s to see when charging has completed. 

 

Note that this project may be downloaded, modified and used by anyone, provided the application is non-profit. If this project, or any derivations of it are to be used for commercial purposes, this may only occur when consent is given by the creators. 
